from __future__ import unicode_literals

from django.db import models
from .thumbs import ImageWithThumbsField
# Create your models here.

class Profil(models.Model):
    util=models.CharField(max_length=256, blank=True)
    photo=ImageWithThumbsField(upload_to='photo', null=True, sizes=((300,300),(200,200)))